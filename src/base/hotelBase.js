var hotelBase = function () {
    var hotelPO = require('../pageObject/hotelPO.js');
    var utilityObj = require('../lib/utility.js')
    var dataProvider = require('../lib/dataProvider.js');
    var customLogger = require('../lib/customLogger.js');
    var loggers = customLogger.logger("Hotelbase");

    /**
     * This method use to navigate to hotel website.
     *
     * @author snarottam
     * @param  url
     */
    this.navigateToHotelWebsite = function (url) {
        utilityObj.getUrl(url);
    }

    this.returnHotelWebPageTitle = function () {
        return hotelPO.returnPageTitle();
    }

    /**
     * This method use to enter to hotel info.
     *
     * @author snarottam
     * @param dataProviderObj
     */
    this.enterHotelInfo = function (dataProviderObj) {
        hotelPO.isSignUpButtonPresent().then(function () {
            loggers.info("Registering new Hotel");
            hotelPO.clickSignUp();
            hotelPO.setName(dataProviderObj.hname);
            hotelPO.setStreet(dataProviderObj.address);
            hotelPO.setCity(dataProviderObj.city);
            hotelPO.setState(dataProviderObj.state);
            hotelPO.setZip(dataProviderObj.zip);
            hotelPO.setCountry(dataProviderObj.country);
            hotelPO.setUrl(dataProviderObj.website);
        });
    }

    /**
     * This method use to enter to hotel contact info.
     *
     * @author snarottam
     * @param dataProviderObj
     */
    this.enterContactInfo = function (dataProviderObj) {
        hotelPO.isContactInfoPresent().then(function () {
            hotelPO.setFirstName(dataProviderObj.firstName);
            hotelPO.setLastName(dataProviderObj.lastName);
            hotelPO.setTitle(dataProviderObj.role);
            hotelPO.setEmail(dataProviderObj.email);
            hotelPO.setPhone(dataProviderObj.phone);
            hotelPO.selectAboutUs(dataProviderObj.aboutus);
            hotelPO.clcikSubmit();
        })
    }
    /**
     * This method use to verify success message present or not.
     *
     * @author narottam
     * @return boolean
     */
    this.verifySuccessMessagePresent = function () {
        return hotelPO.returnIsSuccesMessageDisplayed();
    }
}
module.exports = new hotelBase();






