describe('New Hotel Registration', function () {
    var hotelBase = require('../base/hotelBase.js');
    var customLogger = require('../lib/customLogger.js');
    var logger = customLogger.logger("hotelRegistartionTest");
    var dataProvider = require('../lib/dataProvider.js');

    beforeEach(function () {
        browser.driver.ignoresynchronization = true;
    });

    /**
     * This Test case used to verify navigation to hotel website by using hotelBase.
     *
     * @author narottam
     */
    it('Navigate Hotel Website', function () {
        var dataProviderObj = dataProvider.readDataProvider('../hoteltonight-automation/src/testData/registration.json', 'login');
        logger.info('Navigate to Hotel Website');
        hotelBase.navigateToHotelWebsite(dataProviderObj.url);
        hotelBase.returnHotelWebPageTitle().then(function (text) {
            expect(hotelBase.returnHotelWebPageTitle()).toEqual(dataProviderObj.PageTitle);
            if (text == dataProviderObj.PageTitle) {
                logger.info('We have got to the Hotel page');
            } else {
                logger.error('We dont see the correct page');
            }
            ;
        });
    });

    /**
     * This Test case used to register new hotel by using hotelBase.
     *
     * @author narottam
     */
    it('Register New Hotel', function () {
        var dataProviderObj = dataProvider.readDataProvider('../hoteltonight-automation/src/testData/registration.json', 'registration');
        hotelBase.enterHotelInfo(dataProviderObj);
        hotelBase.enterContactInfo(dataProviderObj);
        hotelBase.verifySuccessMessagePresent().then(function (present) {
            expect(hotelBase.verifySuccessMessagePresent()).toBe(true);
            if(present){
                logger.info("New Hotel Registration successful");
            }else{
                logger.info("New Hotel Registration unsuccessful");
            }
        });

    });
});
