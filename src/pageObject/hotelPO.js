/**
 * This file contanis pageobjects for Hotel registration page.
 *
 * @author narottam
 */

var hotelPO = function () {
    var utility = require('../lib/utility.js');
    var signUp = element(by.linkText('Sign up now'));
    var registrationPageText = element(by.xpath(".//*[@id='contact']/h2"));
    var company = element(by.id('company'));
    var street = element(by.name('street'));
    var city = element(by.id('city'));
    var state = element(by.id('state'));
    var zip = element(by.id('zip'));
    var country = element(by.id('country'));
    var url = element(by.id('URL'));
    var contactInfo=element(by.xpath(".//*[@id='leads']/h2[2]"));
    var firstName = element(by.id('first_name'));
    var lastName = element(by.id('last_name'));
    var title = element(by.id('title'));
    var email = element(by.id('email'));
    var phone = element(by.id('phone'));
    var aboutus = element(by.id('00NE0000004GuNf'));
    var submit = element(by.name('submit'));
    var successMessage=element(by.xpath(".//*[@id='contact']/h2"));

    this.returnPageTitle = function () {
        return browser.getTitle();
    }
    this.clickSignUp = function () {
        utility.browserWait(signUp, 'signUp');
        signUp.click();
    };
    this.isSignUpButtonPresent = function () {
        utility.browserWait(signUp, 'signUp');
        return signUp.isDisplayed();
    };
    this.isContactInfoPresent = function () {
        utility.browserWait(contactInfo, 'contactInfo');
        return contactInfo.isDisplayed();
    };
    this.returnRegistraionPageText = function () {
        utility.browserWait(registrationPageText, 'registrationPageText');
        return registrationPageText.getText();
    };

    this.returnIsSuccesMessageDisplayed = function () {
        utility.browserWait(successMessage, 'successMessage');
        return successMessage.isDisplayed();
    };

    this.setName = function (companyName) {
        utility.browserWait(company, 'company');
        company.clear();
        company.sendKeys(companyName);
    };

    this.setStreet = function (streetName) {
        utility.browserWait(street, 'street');
        street.clear();
        street.sendKeys(streetName);
    };

    this.setCity = function (cityName) {
        utility.browserWait(city, 'city');
        city.clear();
        city.sendKeys(cityName);
    };

    this.setState = function (stateName) {
        utility.browserWait(state, 'state');
        state.clear();
        state.sendKeys(stateName);
    };
    this.setZip = function (zipCode) {
        utility.browserWait(zip, 'zip');
        zip.clear();
        zip.sendKeys(zipCode);
    };
    this.setCountry = function (countryName) {
        utility.browserWait(country, 'country');
        country.sendKeys(countryName);
    };
    this.setUrl = function (hUrl) {
        utility.browserWait(url, 'url');
        url.clear();
        url.sendKeys(hUrl);
    };

    this.setFirstName = function (fName) {
        utility.browserWait(firstName, 'firstName');
        firstName.clear();
        firstName.sendKeys(fName);
    };
    this.setLastName = function (lName) {
        utility.browserWait(lastName, 'lastName');
        lastName.clear();
        lastName.sendKeys(lName);
    };
    this.setTitle = function (hTitle) {
        utility.browserWait(title, 'title');
        title.clear();
        title.sendKeys(hTitle);
    };
    this.setEmail = function (hEmail) {
        utility.browserWait(email, 'email');
        email.clear();
        email.sendKeys(hEmail);
    };
    this.setPhone = function (hEmail) {
        utility.browserWait(phone, 'phone');
        phone.clear();
        phone.sendKeys(hEmail);
    };
    this.selectAboutUs = function (hAboutus) {
        utility.browserWait(aboutus, 'aboutus');
        aboutus.sendKeys(hAboutus);
    };
    this.clcikSubmit = function () {
        utility.browserWait(submit, 'submit');
        submit.click();
    };
};
module.exports = new hotelPO();