module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            files: ['Gruntfile.js', 'specs/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        protractor: {
            stag: {
                options: {
                    keepAlive: true,
                    configFile: "ProtractorConf/configuration.js",
                    args: {
                        params: {
                            user: "CS",
                            env: "stag"
                        }
                    }
                },
                singlerun: {},
                auto: {
                    keepAlive: true,
                    options: {
                        args: {
                            seleniumPort: 4444
                        }
                    }
                }
            },
            qa: {
                options: {
                    keepAlive: true,
                    configFile: "protractorConf/configuration.js",
                    args: {
                        params: {
                            user: "CS",
                            env: "qa"
                        }
                    }
                },
                singlerun: {},
                auto: {
                    keepAlive: true,
                    options: {
                        args: {
                            seleniumPort: 4444
                        }
                    }
                }
            }
        },
        shell: {
            options: {
                stdout: true
            },
            protractor_install: {
                command: 'node ./node_modules/protractor/bin/webdriver-manager update'
            },
            npm_install: {
                command: 'npm install'
            }
        }
    });

    grunt.loadNpmTasks('grunt-protractor-runner');

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-shell-spawn');

    grunt.registerTask('install', ['shell:npm_install', 'shell:protractor_install']);

    grunt.registerTask('default', ['jshint', 'protractor:stag']);
    grunt.registerTask('qa', ['jshint', 'protractor:qa']);
};